import {Map, fromJS} from 'immutable';
import {expect} from 'chai';

import reducer from '../src/reducer';

describe('reducer', () => {

	it('handles SET_ENTRIES', () => {
		const initialState = Map();
		const action = {type: 'SET_ENTRIES', entries: ['Map 1']}
		const nextState = reducer(initialState, action);

		expect(nextState).to.equal(fromJS({
			entries: ['Map 1']
		}));
	});

	it('handles NEXT', () => {
		const initialState = fromJS({
			entries: ['Map 1', 'Map 2']
		});
		const action = {type: 'NEXT'};
		const nextState = reducer(initialState, action);

		expect(nextState).to.equal(fromJS({
			vote: {
				pair: ['Map 1', 'Map 2']
			},
			entries: []
		}));
	});

 it('handles VOTE', () => {
    const initialState = fromJS({
      vote: {
        pair: ['map1', 'map2']
      },
      entries: []
    });
    const action = {type: 'VOTE', entry: 'map1'};
    const nextState = reducer(initialState, action);

    expect(nextState).to.equal(fromJS({
      vote: {
        pair: ['map1', 'map2'],
        tally: {map1: 1}
      },
      entries: []
    }));
  });

	it('has an initial state', () => {
		const action = {type: 'SET_ENTRIES', entries: ['map1']};
		const nextState = reducer(undefined, action);
		expect(nextState).to.equal(fromJS({
			entires: ['map1']
		}));
	});

	it('can be used with reduce', () => {
		const actions = [
			{type: 'SET_ENTRIES', entries: ['map1', 'map2']},
			{type: 'NEXT'},
			{type: 'VOTE', entry: 'map1'},
			{type: 'VOTE', entry: 'map2'},
			{type: 'VOTE', entry: 'map1'},
			{type: 'NEXT'}
		];
		const finalState = actions.reduce(reducer, Map());

		expect(finalState).to.equal(fromJS({
			winner: 'map1'
		}));
	});

});