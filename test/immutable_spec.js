import {expect} from 'chai';
import {List, Map} from 'immutable';

describe('immutability', () => {

  // ...

  describe('a tree', () => {

		function addMap(currentState, map) {
		  return currentState.update('maps', maps => maps.push(map));
		}

    it('is immutable', () => {
      let state = Map({
        maps: List.of('Map 1', 'Map 2')
      });
      let nextState = addMap(state, 'Map 3');

      expect(nextState).to.equal(Map({
        maps: List.of(
          'Map 1',
          'Map 2',
          'Map 3'
        )
      }));
      expect(state).to.equal(Map({
        maps: List.of(
          'Map 1',
          'Map 2'
        )
      }));
    });

  });

});