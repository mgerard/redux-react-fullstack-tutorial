import {List, Map} from 'immutable';
import {expect} from 'chai';
import {setEntries, next, vote} from '../src/core';

describe('application logic', () => {

  describe('next', () => {

    it('takes the next two entries under vote', () => {
      const state = Map({
        entries: List.of('Map 1', 'Map 2', 'Map 3')
      });
      const nextState = next(state);
      expect(nextState).to.equal(Map({
        vote: Map({
          pair: List.of('Map 1', 'Map 2')
        }),
        entries: List.of('Map 3')
      }));
    });

	  it('puts winner of current vote back to entries', () => {
	    const state = Map({
	      vote: Map({
	        pair: List.of('Map 1', 'Map 2'),
	        tally: Map({
	          'Map 1': 4,
	          'Map 2': 2
	        })
	      }),
	      entries: List.of('Map 4', 'Map 5', 'Map 6')
	    });
	    const nextState = next(state);
	    expect(nextState).to.equal(Map({
	      vote: Map({
	        pair: List.of('Map 4', 'Map 5')
	      }),
	      entries: List.of('Map 6', 'Map 1')
	    }));
	  });

	  it('puts both from tied vote back to entries', () => {
	    const state = Map({
	      vote: Map({
	        pair: List.of('Map 1', 'Map 2'),
	        tally: Map({
	          'Map 1': 3,
	          'Map 2': 3
	        })
	      }),
	      entries: List.of('Map 4', 'Map 5', 'Map 6')
	    });
	    const nextState = next(state);
	    expect(nextState).to.equal(Map({
	      vote: Map({
	        pair: List.of('Map 4', 'Map 5')
	      }),
	      entries: List.of('Map 6', 'Map 1', 'Map 2')
	    }));
	  });

	  it('marks winner when just one entry left', () => {
	    const state = Map({
	      vote: Map({
	        pair: List.of('Map 1', 'Map 2'),
	        tally: Map({
	          'Map 1': 4,
	          'Map 2': 2
	        })
	      }),
	      entries: List()
	    });
	    const nextState = next(state);
	    expect(nextState).to.equal(Map({
	      winner: 'Map 1'
	    }));
	  });

  });

  describe('setEntries', () => {

    it('adds the entries to the state', () => {
      const state = Map();
      const entries = List.of('Map 1', 'Map 2');
      const nextState = setEntries(state, entries);
      expect(nextState).to.equal(Map({
        entries: List.of('Map 1', 'Map 2')
      }));
    });

  });

  describe('vote', () => {

  	it('creates a tally for the voted entry', () => {
  		const state = Map({
  			pair: List.of('Map 1', 'Map 2')
  		});
  		const nextState = vote(state, 'Map 1');
  		expect(nextState).to.equal(Map({
  			pair: List.of('Map 1', 'Map 2'),
	      tally: Map({
	        'Map 1': 1
	      })
  		}));
  	});

  	it('add to existing tally for the voted entry', () => {
  		const state = Map({
  			pair: List.of('Map 1', 'Map 2'),
	      tally: Map({
	        'Map 1': 3,
	        'Map 2': 2
	      })
  		});
  		const nextState = vote(state, 'Map 1');
  		expect(nextState).to.equal(Map({
	      pair: List.of('Map 1', 'Map 2'),
	      tally: Map({
	        'Map 1': 4,
	        'Map 2': 2
	      })
  		}));
  	});

  });

});
